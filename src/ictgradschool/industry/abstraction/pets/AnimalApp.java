package ictgradschool.industry.abstraction.pets;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import ictgradschool.industry.abstraction.farmmanager.animals.Animal;

/**
 * Main program.
 */
public class AnimalApp {

    public void start() {

        IAnimal[] animals = new IAnimal[3];


        // TODO Populate the pets array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();
        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the pets in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.

        for (int i = 0; i < list.length; i++) {
            System.out.println(list[i].myName() + " says " + list[i].sayHello());
            System.out.println(list[i].myName() + " is a " + list[i].isMammal());
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs");

            if (list[i] instanceof IFamous){
                IFamous other = (IFamous) list[i];
                System.out.println("This is a famous name of my animal type:" +other.famous() );

            }

        }



    }

    public static void main(String[] args) {
        new AnimalApp().start();

    }
}
